# mac-superdrive-spark-store

## 介绍

[Mac Superdrive 规则](https://gist.github.com/yookoala/818c1ff057e3d965980b7fd3bf8f77a6)，供[星火应用商店](https://spark-app.store/)上架

## 使用说明

于仓库根目录执行以下命令：

```bash
$ ./build-deb.sh
```

## 许可证

[MIT](./LICENSE) （仅限本仓库文档，不包含 Mac Superdrive 规则本身）

Mac Superdrive 规则未标明许可证，默认 [Koala Yeung](https://gist.github.com/yookoala) 版权所有
