#!/bin/bash -e

rm -rf ./dl-assets/
mkdir -p ./dl-assets/
pushd dl-assets/
wget https://gist.githubusercontent.com/yookoala/818c1ff057e3d965980b7fd3bf8f77a6/raw/ecfb680c225dfdb6880fda02f37cd63597f31715/90-mac-superdrive.rules
popd
dpkg-buildpackage -us -uc
lintian ../mac-superdrive_20220407~spark1_all.deb
